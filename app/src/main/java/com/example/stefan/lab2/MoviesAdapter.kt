package com.example.stefan.lab2

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.movies_adapter.view.*

/**
 * Created by stefan on 12.11.17.
 */
class MoviesAdapter(private val items: List<Movie>, private val listener: (Movie) -> Unit) :
        RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.movies_adapter))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)

    override fun getItemCount() = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Movie, listener: (Movie) -> Unit) = with(itemView) {
            title.text = item.name
            image.loadUrl(item.imageUrl)
            description.text = item.description
            year.text = item.year
            director.text = item.director
            setOnClickListener { listener(item) }
        }
    }

}

