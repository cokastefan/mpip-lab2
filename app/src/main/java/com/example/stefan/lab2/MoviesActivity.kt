package com.example.stefan.lab2

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_movies.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import java.net.URL

class MoviesActivity : AppCompatActivity() {

    private lateinit var mMovies: List<Movie>
    private lateinit var mMovieDetails: MutableList<Movie>
    private lateinit var mAdapter: MoviesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)

        mMovies = emptyList()
        mMovieDetails = mutableListOf()

        search_button.setOnClickListener { search() }

        list.layoutManager = LinearLayoutManager(this)

        mAdapter = MoviesAdapter(mMovieDetails) {
            val i = Intent(this@MoviesActivity, MovieDetailsActivity::class.java)
            i.putExtra("movie", it)
            startActivity(i)
        }
        list.adapter = mAdapter
    }

    private fun search() {
        mMovieDetails.clear()
        list.adapter.notifyDataSetChanged()
        progress_bar.visibility = View.VISIBLE
        doAsync {
            val movie: String = search_src_text.text.toString()
            val result = URL("http://www.omdbapi.com/?s=$movie&apikey=cb0c41&r=json").readText()
            uiThread {
                readJson(result)
            }
        }
    }

    private fun readJson(result: String) {
        val parser = JsonParser()
        val json: JsonElement = parser.parse(result)
        val resultJson: JsonObject = json.asJsonObject
        val resultArray = resultJson.get("Search")

        if (resultArray == null) {
            toast("No results found")
            return
        }

        mMovies = Gson().fromJson(resultArray, object : TypeToken<List<Movie>>() {}.type)

        getDetails()

    }

    private fun getDetails() {
        for (movie in mMovies) {
            doAsync {
                val result = URL("http://www.omdbapi.com/?i=${movie.imdbId}&plot=short&apikey=cb0c41&r=json").readText()
                uiThread {
                    addDetails(result)
                    Log.d("details", mMovieDetails.toString())
                }
            }
        }
    }

    private fun addDetails(result: String) {
        val gson = Gson()

        val newMovie: Movie = gson.fromJson(result, Movie::class.java)
        mMovieDetails.add(newMovie)
        progress_bar.visibility = View.GONE
        list.adapter.notifyDataSetChanged()
    }


}
